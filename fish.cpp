///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.cpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/26/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "fish.hpp"

using namespace std;

namespace animalfarm {

// Print out information for animals then fish
void Fish::printInfo() {
        Animal::printInfo();
        cout << "   Scale Color = [" << colorName( scaleColor ) << "]" << endl;
        cout << "   Favorite Temperature = [" << favoriteTemperature << "]" << endl;
}


} // namespace animalfarm

