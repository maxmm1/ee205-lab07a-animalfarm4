///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Header file for node
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03/29/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

//#include "animal.hpp"


namespace animalfarm {

class Node {

   friend class SingleLinkedList;

   protected:
      Node* next = nullptr;

      //Animal* newAnimal = NULL;

};

}
