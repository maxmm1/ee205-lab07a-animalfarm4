///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
///
/// @file list.cpp
/// @version 1.0
///
/// Implementation file for list
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03/29/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <list>

#include "node.hpp"
#include "list.hpp"


namespace animalfarm {


// Return true if the list is empty, false if there’s anything in the list
bool SingleLinkedList::empty() const {

   if ( head == nullptr )
      return 1;
   else
      return 0;

};


// Add newNode to the front of the list
void SingleLinkedList::push_front( Node* newNode ) {

   newNode -> next = head;
   head = newNode;

};


// Remove a node from the front of the list. If the list is already empty, return nullptr
Node* SingleLinkedList::pop_front() {

   if ( head == nullptr ) {
      return nullptr;
   }
 
   Node *temp = head;
   head = head -> next;

   return temp;

};


// Return the very first node from the list. Don’t make any changes to the list.
Node* SingleLinkedList::get_first() const {

   return head;

};


// Return the node immediately following currentNode
Node* SingleLinkedList::get_next( const Node* currentNode ) const {

   const Node* temp = currentNode;
   Node* nextNode = temp -> next;
   return nextNode;

};


// Return the number of nodes in the list
unsigned int SingleLinkedList::size() const {

   unsigned int sum = 0;
   Node* temp = head;

   while( temp != nullptr ) {
      sum++;
      temp = temp -> next;
   }

   return sum;
};



}
