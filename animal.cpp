///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;

namespace animalfarm {

// Print out animal information	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


// Convert Gender enum into string
string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male");      break;
      case FEMALE:  return string("Female");    break;
      case UNKNOWN: return string("Unknown");   break;
   }

   return string("Really, really Unknown");
};

	
// Convert Color enum into string
string Animal::colorName (enum Color color) {
   switch (color) {
      case RED:		return string("Red");      break;
      case ORANGE:	return string("Orange");   break;
      case YELLOW:	return string("Yellow");   break;
      case GREEN:	   return string("Green");    break;
      case BLUE:	   return string("Blue");     break;
      case WHITE:	   return string("White");    break;
      case BLACK:	   return string("Black");    break;
      case BROWN:	   return string("Brown");    break;
      case SILVER:	return string("Silver");   break;
   }

   return string("Unknown");
};


// Generate random number between 65 and 90 (Uppercase ASCII values)
int Animal::randUppercase() {
   int upper = 65 + rand() % (90 - 65 + 1);

   return upper;
}


// Generate random number between 65 and 90 (Lowercase ASCII values)
int Animal::randLowercase() {
   int lower = 97 + rand() % (122 - 97 + 1);

   return lower;
}


// Generate random name between 4 and 9 characters
const string Animal::getRandomName() {
   int length = 4 + rand() % (9 - 4 + 1);	// Create random length of name
   std::string randname;

   randname.insert( 0, 1, randUppercase() );	// Create random Uppercase letter at first position
   for ( int i = 1; i < length; i++) {
      randname.insert( i, 1, randLowercase() );	// Fill rest of name with Lowercase letters
   }

   return randname;
}


// Generate random gender
const Gender Animal::getRandomGender() {
   int g = rand() % ( 1 + 1 );
   Gender gender;

   switch (g) {
      case 0:	gender = MALE;		break;
      case 1:	gender = FEMALE;	break;
   }

   return gender;
}


// Generate random color
const Color Animal::getRandomColor() {
   int c = rand() % ( 9 );
   Color color;

   switch (c) {
      case 0:   color = RED;     break;
      case 1:   color = ORANGE;  break;
      case 2:   color = YELLOW;  break;
      case 3:   color = GREEN;   break;
      case 4:   color = BLUE;    break;
      case 5:   color = WHITE;   break;
      case 6:   color = BLACK;   break;
      case 7:   color = BROWN;   break;
      case 8:   color = SILVER;  break;
   }

   return color;
}


// Generate random boolean value
const bool Animal::getRandomBool() {
   bool b = rand() % ( 2 );

   return b;

}


// Generate random weight between two float values
const float Animal::getRandomWeight( const float from, const float to ) {

   float random = ((float) rand()) / (float) RAND_MAX;
   float diff = to - from;
   float r = random * diff;

   return from + r;
}


// Generate random animal
Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   int i = rand() % ( 6 ); 
   switch (i) {
      case 0:  newAnimal = new Cat     ( RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER );   break;
      case 1:  newAnimal = new Dog     ( RANDOM_NAME, RANDOM_COLOR, RANDOM_GENDER );   break;
      case 2:  newAnimal = new Nunu    ( RANDOM_BOOL, RED, RANDOM_GENDER );            break;
      case 3:  newAnimal = new Aku     ( RANDOM_WEIGHT, SILVER, RANDOM_GENDER );       break;
      case 4:  newAnimal = new Palila  ( RANDOM_NAME, YELLOW, RANDOM_GENDER );         break;
      case 5:  newAnimal = new Nene    ( RANDOM_NAME, BROWN, RANDOM_GENDER );          break;
   }
   
   return newAnimal;
}


// Constructor to print .
Animal::Animal(void) {
   cout << ".";
}


// Destructor to print x
Animal::~Animal(void) {
   cout << "x";
}


} // namespace animalfarm



